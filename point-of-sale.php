<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>نظام ادارة المبيعات مؤسسة القرنفل</title>
    <meta name="description" content="Inventory &amp; Point of Sale System">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/Pricing-Centered-badges.css">
    <link rel="stylesheet" href="assets/css/Pricing-Centered-icons.css">
</head>

<body id="page-top">
    <div id="wrapper">
        <?php
            include_once 'DBconfig.php';
            include_once 'functions/authentication.php';
            include_once 'functions/sidebar.php';
        ?>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
            <?php include_once 'navbar.php'; ?>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body text-center p-4">
                                    <h6 class="text-uppercase text-muted card-subtitle">المجموع</h6>
                                    <h4 class="display-4 fw-bold card-title">د <?php include_once 'functions/pos-total.php'; ?></h4>
                                </div>
                                <div class="card-footer p-4">
                            
                                <form class="text-center" method="post">
                                    <div class="mb-3"><button class="btn btn-primary d-block w-100" type="button" data-bs-target="#purchase" data-bs-toggle="modal">اصدار فاتورة &nbsp;</button></div>
                                </form>
                            
                                    <div class="card" style="margin-top: 16px;">
                                        <div class="card-header py-3">
                                            <p class="text-primary m-0 fw-bold">العناصر</p>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive table mt-2" id="dataTable-1" role="grid" aria-describedby="dataTable_info">
                                                <table class="table table-hover table-bordered my-0" id="dataTable">
                                                    <thead>
                                                        <tr>
                                                            <th> رقم المنتج</th>
                                                            <th>اسم المنتج </th>
                                                            <th>الكمية</th>
                                                            <th>السعر</th>
                                                            <?php
                                                        if(!isset($_GET['invoiceId']))
                                                         echo'   <th>خيار</th>';
                                                            ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                                        <?php
                                                        if(!isset($_GET['invoiceId']))
                                                            include_once 'functions/pos-history.php';
                                                        else
                                                            include_once 'functions/show-invoice.php';
                                                         ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      <?php  if(!isset($_GET['invoiceId'])){ ?>
                        <div class="col-md-6">
                            <div class="card shadow">
                                <div class="card-header py-3">
                                    <p class="text-primary m-0 fw-bold">المنتجات</p>
                                </div>
                                <div class="card-body">
                                    
                                    <div class="table-responsive table mt-2" id="dataTable-2" role="grid" aria-describedby="dataTable_info">
                                        <table class="table table-hover table-bordered my-0" id="dataTable">
                                            <thead>
                                                <tr>
                                                    <th> رقم المنتج</th>
                                                    <th>اسم المنتج </th>
                                                    <th>الكمية</th>
                                                    <th>السعر</th>
                                                    <th>خيار</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php include_once 'functions/pos-view-products.php'; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                      <?php } ?>
                    </div>
                </div>
            </div>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    <div class="modal fade" role="dialog" tabindex="-1" id="purchase">
        <div class="modal-dialog" role="document">
        <?php  if(!isset($_GET['invoiceId'])){ ?>
            <form action="functions/pos-transaction.php" method="post">
                <?php }else{?>
                    <form action="functions/pos-transaction.php?invoiceId=<?=$_GET['invoiceId']?>" method="post">
                <?php }?>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">نظام المبيعات</h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class="modal-body">
                        <p>تأكيد الفاتورة &nbsp;</p>
                        <div class="card">
                            <div class="card-body text-center p-4">
                                <h6 class="text-uppercase text-muted card-subtitle">المجموع</h6>
                                <h4 class="display-4 fw-bold card-title">د <?php include 'functions/pos-total.php'; ?></h4>
                                <div class="mb-3"><input class="form-control" type="number" value="0" name="discount" placeholder="الخصم "></div>
                                <div class="mb-3"><input class="form-control" type="number" value="<?php include 'functions/pos-total.php'; ?>" name="amount" placeholder="السعر النهائي"></div>
                                <input type="hidden" name="total_sales" value="<?php include 'functions/pos-total.php'; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">اغلاق</button>
                    <button class="btn btn-primary" type="submit">ارسال</button></div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" role="dialog" tabindex="-1" id="confirmation">
        <div class="modal-dialog" role="document">
            <form action="functions/pos-remove-history.php" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">رسالة تأكيد</h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class="modal-body">
                        <p>هل انت متأكد من حذف العنصر</p>
                        <input type="hidden" name="history_id">
                        <input type="hidden" name="product_id">
                        <input type="hidden" name="product_qty">

                    </div>
                    <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">اغلاق</button><button class="btn btn-danger" type="submit">حذف</button></div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" role="dialog" tabindex="-1" id="add-item">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">اضافة عنصر  </h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <p>الكمية</p>
                    <form class="text-center" action="functions/pos-add-item.php" method="post">
                        <input type="hidden" name="product_id">
                        <div class="mb-3"><input class="form-control" type="number" name="item_qty" value="1" placeholder="Quantity" required></div>
                        <div class="mb-3"><button class="btn btn-primary d-block w-100" type="submit">حفظ </button></div>
                    </form>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">اغلاق</button></div>
            </div>
        </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script>
        
        $('button[data-bs-target="#add-item"]').on('click', function() {
        // Get the user ID from the data attribute.
        var product_id = $(this).data('product-id');
        console.log(product_id);
        // Set the value of all input fields with the name "userid" to the user ID.
        $('input[name="product_id"]').each(function() {
            $(this).val(product_id);
        });
        });

        $('button[data-bs-target="#confirmation"]').on('click', function() {
        // Get the user ID from the data attribute.
        var product_id = $(this).data('product-id');
        var history_id = $(this).data('history-id');
        var qty = $(this).data('qty');
        

        console.log(product_id);
        // Set the value of all input fields with the name "userid" to the user ID.
        $('input[name="history_id"]').each(function() {
            $(this).val(history_id);
        });

        $('input[name="product_id"]').each(function() {
            $(this).val(product_id);
        });


        $('input[name="product_qty"]').each(function() {
            $(this).val(qty);
        });
        });
    </script>

    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="assets/js/theme.js"></script>
</body>

</html>
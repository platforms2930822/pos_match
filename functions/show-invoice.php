<?php

include_once 'DBconfig.php';
// Get all data from the products table
$invoiceId=$_GET['invoiceId'];
$sql = 'SELECT * FROM history WHERE transaction_id = '.$invoiceId.'';
$stmt = $db->prepare($sql);
$stmt->execute();
$results = $stmt->fetchAll();


// Loop through the results and add them to the table
foreach ($results as $row) {
?>
    <tr>
        <td>&nbsp;<?php echo $row['product_id']; ?></td>
        <td><?php echo $row['product_name']; ?></td>
        <td><?php echo $row['qty']; ?></td>
        <td><?php echo $row['price']; ?></td>
        <td class="removeItem d-print-none">
            <button class="btn btn-danger btn-icon-split" type="button" data-bs-target="#confirmation" data-bs-toggle="modal" data-history-id="<?php echo $row['id']; ?>" data-product-id="<?php echo $row['product_id']; ?>" data-qty="<?php echo $row['qty']; ?>"><span class="text-white-50 icon"><i class="fas fa-trash"></i></span><span class="text-white text">حذف</span></button>
        </td>
    </tr>
<?php
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Inventory &amp; Point of Sale System</title>
    <meta name="description" content="Inventory &amp; Point of Sale System">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap">
    <link rel="stylesheet" href="assets/css/Pricing-Centered-badges.css">
    <link rel="stylesheet" href="assets/css/Pricing-Centered-icons.css">
    <style>
            @media print {
      body {
        direction: rtl;
      }

      .text-direction-rtl {
        text-align: right !important;
      }
    }

        .removeItem{
            display: none;
        }
    </style>
</head>
<?php
 $invoiceId=$_GET['invoiceId'];
 include_once 'DBconfig.php';
if(isset($_POST['save_location']))
{
    $sql = "UPDATE client_detiles SET name = :name , stat = :stat, location=:location, phone=:phone ,notes=:notes,date=:date WHERE 	transaction_id = :invoiceId ";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':name',$_POST['name'] );
    $stmt->bindParam(':stat',$_POST['stat'] );
    $stmt->bindParam(':location',$_POST['location'] );
    $stmt->bindParam(':phone',$_POST['phone'] );
    $stmt->bindParam(':notes',$_POST['notes'] );
    $stmt->bindParam(':invoiceId',$_GET['invoiceId'] );
    $stmt->bindParam(':date',$_POST['date'] );
    $stmt->execute();
}


 $sql = 'SELECT * FROM client_detiles WHERE transaction_id = :transaction_id';
 $stmt = $db->prepare($sql);
 $stmt->bindParam(':transaction_id', $invoiceId );
 $stmt->execute();
 $client_detiles = $stmt->fetch();
;
?>
<body>
    <div class="container py-4 py-xl-5">
        <div class="row mb-5">
            <div class="col-md-8 col-xl-6 text-center mx-auto">
                <h2> نظام المبيعات - مؤسسة القرنفل </h2>
            </div>
        </div>
        <div class="row gy-4 gy-xl-0 row-cols-1 row-cols-md-2 row-cols-xl-3 d-xl-flex justify-content-center align-items-xl-center text-direction-rtl" style="direction: rtl;">
            <div class="col">
                <div class="card border-primary border-2  text-direction-rtl" id="printing" style="direction: rtl;">
                    <div class="card-body text-center p-4"><span class="badge rounded-pill bg-primary position-absolute top-0 start-50 translate-middle text-uppercase mt-2" >فاتورة</span>
                        <h6 class="text-uppercase text-muted card-subtitle mt-4">وكالة منتجات الماتشا
"مؤسسة القرنفل "</h6>
                        <!-- <h4 class="display-4 fw-bold card-title">د<?php echo $_GET['discounted_sales']; ?></h4> -->
                    </div>
                    <div class="card-footer p-4">
                        <div>
                        <div class="card-body">
                                <div class="table-responsive table mt-2  text-direction-rtl" id="dataTable-1" role="grid" aria-describedby="dataTable_info">
                                    <div class="d-print-none">
                                        <form method="post">
                                            <div class="mb-3"><input  class="form-control" name="name" oninput="updateText('name')" type="text" value="<?= $client_detiles['name']??''?>" placeholder=" اسم المستلم " /></div>
                                            <div class="mb-3"><input class="form-control" name="location" oninput="updateText('location')" type="text" value="<?= $client_detiles['location'] ??''?>"  placeholder=" عنوان المستلم " /></div>
                                            <div class="mb-3"><input class="form-control" name="stat" oninput="updateText('stat')" type="text" value="<?= $client_detiles['stat']??''?>"  placeholder=" محافظة المستلم " /></div>
                                            <div class="mb-3"><input class="form-control" name="phone" oninput="updateText('location')" type="text" value="<?= $client_detiles['phone']?? '' ?>" placeholder=" رقم هاتف المستلم " /></div>
                                            <div class="mb-3"><input class="form-control" name="date" oninput="updateText('location')" type="date" value="<?= $client_detiles['date']?? '' ?>" placeholder=" موعد التسليم" /></div>
                                            <div class="mb-3"><textarea class="form-control" name="notes"  placeholder=" ملاحظات" ><?= $client_detiles['notes']?? '' ?></textarea></div>
                                            <div class="mb-3"><input class="form-control" name="save_location" oninput="updateText('location')" type="submit" value="ارسال" placeholder=" رقم هاتف المستلم " /></div>
                                        </form>
                                    </div>
                                    <p class=" text-direction-rtl">اسم العميل: <span id="nameP"> <?= $client_detiles['name']  ?></span></p>
                                    <p  class=" text-direction-rtl">عنوان :<span id="locationP"><?= $client_detiles['location'] ??'' ?></span></p>
                                    <p  class=" text-direction-rtl"> المحافظة:<span id="statP"><?= $client_detiles['stat']??'' ?></span></p>
                                    <p  class=" text-direction-rtl"> موعد التسليم:<span id="statP"><?= $client_detiles['date']??'' ?></span></p>
                                    <p  class=" text-direction-rtl"> رقم الهاتف :<span id="phoneP"><?= $client_detiles['phone']??'' ?></span> </p>
                                    <p  class=" text-direction-rtl"> الملاحظات: <span id="phoneP"><?= $client_detiles['notes']??'' ?></span> </p>


                                    <table class="table table-hover table-bordered my-0" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th> رقم المنتج</th>
                                                <th>اسم المنتج </th>
                                                <th>الكمية</th>
                                                <th>السعر</th>

                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            include_once 'functions/show-invoice.php';
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <ul class="list-unstyled">
                                <li class="d-flex mb-2"><span class="bs-icon-xs bs-icon-rounded bs-icon-primary-light bs-icon me-2"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-check-lg">
                                            <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"></path>
                                        </svg></span><span>السعر  د<?php echo $_GET['sales']; ?></span></li>

                                <li class="d-flex mb-2"><span class="bs-icon-xs bs-icon-rounded bs-icon-primary-light bs-icon me-2"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-check-lg">
                                            <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"></path>
                                        </svg></span><span>الخصم <?php echo $_GET['discount']; ?></span></li>

                                <li class="d-flex mb-2"><span class="bs-icon-xs bs-icon-rounded bs-icon-primary-light bs-icon me-2"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-check-lg">
                                            <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"></path>
                                        </svg></span><span>السعر النهائي <?php echo $_GET['amount']; ?></span></li>

                                <!-- <li class="d-flex mb-2"><span class="bs-icon-xs bs-icon-rounded bs-icon-primary-light bs-icon me-2"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-check-lg">
                                            <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"></path>
                                        </svg></span><span>مجموع الخصم<?php echo $_GET['amount'] - $_GET['discounted_sales']; ?></span></li> -->

                                <li class="d-flex mb-2"><span class="bs-icon-xs bs-icon-rounded bs-icon-primary-light bs-icon me-2"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-check-lg">
                                            <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"></path>
                                        </svg></span><span> تاريخ الفوترة: <?php $date = date('Y-m-d H:i:s');
                                                                            echo $date; ?></span></li>
                            </ul>


                       

                            
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary d-block w-100" style="margin-bottom:10px; margin-top:10px" onclick="printReceipt()">اطبع</button>
                <a class="btn btn-primary d-block w-100" role="button" href="point-of-sale.php">العودة</a>
            </div>
        </div>
    </div>
    <script>
        function updateText(inputId) {
            // Get the input and output elements by their IDs
            var inputElement = document.getElementById(inputId);
            var outputElement = document.getElementById(inputId + 'P');

            // Update the text content of the output element with the input value
            outputElement.textContent = inputElement.value;
        }
    </script>
    </script>
    <script>
        function printReceipt() {
            var contentToPrint = document.getElementById("printing");
            // var printWindow = window.open('', '', 'width=600,height=400');
            var printWindow = window.open('', '_blank','width=600,height=400');
            
            printWindow.document.write('<html><head><title>Print</title></head><body style="direction: rtl;"> <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">');
            printWindow.document.write(contentToPrint.innerHTML);
            printWindow.document.write('<footer class="" style=" position: relative; bottom: -30vh; right:0%;">  <p> رقم هاتف المؤسسة: 0779760605 | 0786461888  موقع الكتروني المؤسسة: www.matchajo.com</p>    </footer></body></html>');
            
            printWindow.document.close(); // necessary for IE >= 10
            printWindow.focus(); // necessary for IE >= 10
            
            printWindow.print();
            printWindow.close();



            // var receipt = document.getElementById('printing');
            // var newWin = window.open('', 'Print-Window');
            // newWin.document.open();
            // newWin.document.write('<html lang="ar"><head><link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css"><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap"><link rel="stylesheet" href="assets/css/Pricing-Centered-bad55ges.css"><link rel="stylesheet" href="assets/css/Pricing-Cente55red-icons.css"></head><body onload="window.print()"><div class="row mb-5" style="direction: rtl;"><div class="col-md-8 col-xl-6 text-center mx-auto" style="direction: rtl;"><h2> مؤسسة القرنفل</h2></div></div>' + receipt.innerHTML + ' <script src="assets//bootstrap//js//bootstrap.min.js"><//script><script src="assets//js/bs-init.js"><//script><script src="assets//js/theme.js"><//script></body></html>');
            // newWin.document.close();
            // newWin.document.close();
            setTimeout(function() {
                newWin.close();
            }, 10);
        }
    </script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="assets/js/theme.js"></script>
</body>

</html>
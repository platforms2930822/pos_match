<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Users - نظام المبيعات</title>
    <meta name="description" content="Inventory &amp; Point of Sale System">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/Pricing-Centered-badges.css">
    <link rel="stylesheet" href="assets/css/Pricing-Centered-icons.css">
</head>

<body id="page-top">
    <div id="wrapper">
        <?php
        include_once 'DBconfig.php';
        include_once 'functions/authentication.php';
        include_once 'functions/sidebar.php';
        ?>

        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
           <?php include_once 'navbar.php'; ?>
                <div class="container-fluid">
                    <h3 class="text-dark mb-4"> ادارة المستخدمين&nbsp;</h3>
                    <div class="row">
                        <div class="col-md-6 col-xl-4 mb-4">
                            <div class="card shadow border-start-warning py-2">
                                <div class="card-body">
                                    <div class="row align-items-center no-gutters">
                                        <?php
                                      
                                       

                                        // Get the total number of users.
                                        $sql = "SELECT COUNT(*) FROM users";
                                        $stmt = $db->prepare($sql);
                                        $stmt->execute();
                                        $row = $stmt->fetch();
                                        $total_users = $row['COUNT(*)'];

                                        // Display the total number of users.
                                        echo "<div class=\"col me-2\">
                                                <div class=\"text-uppercase text-info fw-bold text-xs mb-1\"><span>عدد المستخدمين</span></div>
                                                <div class=\"text-dark fw-bold h5 mb-0\"><span>$total_users</span></div>
                                                </div>";

                                        ?>
                                        <div class="col-auto"><i class="fas fa-comments fa-2x text-gray-300"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-sm-flex justify-content-between align-items-center mb-4">
                        <h3 class="text-dark mb-0"></h3><button class="btn btn-primary btn-sm d-none d-sm-inline-block" type="button" data-bs-target="#add-user" data-bs-toggle="modal"><i class="fas fa-user fa-sm text-white-50"></i>&nbsp; انشاء حساب بائع</button>
                    </div>
                    <div class="card shadow">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 fw-bold">قائمة المستخدمين </p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                            </div>
                            <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table class="table table-hover table-bordered my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th> ID</th>
                                            <th>اسم المستخدم</th>
                                            <th>النوع</th>
                                            <th>المنشئ</th>
                                            <th>خيارات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php include_once 'functions/view-users.php'; ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span>جميع الحقوق محفوظة ©  مؤسسة القرنفل 2023</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    <div class="modal fade" role="dialog" tabindex="-1" id="add-user">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">اضافة مستخدم </h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <p>معلومات المستخدم </p>
                    <form class="text-center" action="functions/create-account.php" method="post">
                        <div class="mb-3"><input class="form-control" type="text" name="username" placeholder="اسم المستخدم" minlength="5" pattern="^(?!\s).*$" required></div>
                        <div class="mb-3"><input class="form-control" type="password" name="password" placeholder="الرقم السري" minlength="5" pattern="^(?!\s).*$" required></div>
                        <div class="mb-3"></div>
                        <div class="mb-3"><button class="btn btn-primary d-block w-100" type="submit"> اضافة</button></div>
                    </form>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">اغلاق</button></div>
            </div>
        </div>
    </div>
    <div class="modal fade" role="dialog" tabindex="-1" id="update">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">تغيير كلمة السر </h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <p>User Information</p>
                    <form class="text-center" action="functions/update-account.php" method="post">
                        <input type="hidden" name="userid">
                        <div class="mb-3"><input class="form-control" type="password" name="password" placeholder="الكلمة القديمة" minlength="5" pattern="^(?!\s).*$" required></div>
                        <div class="mb-3"><input class="form-control" type="password" name="new_password" placeholder="تأكيد الكلمة " minlength="5" pattern="^(?!\s).*$" required></div>
                        <div class="mb-3"></div>
                        <div class="mb-3"><button class="btn btn-primary d-block w-100" type="submit">تغيير   </button></div>
                    </form>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">اغلاق</button></div>
            </div>
        </div>
    </div>
    <div class="modal fade" role="dialog" tabindex="-1" id="confirmation">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">تأكيد</h4><button class="btn-close" type="button" aria-label="Close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <p>هل متأكد من حذف المستخدم</p>
                </div>
                <form action="functions/remove-user.php" method="post">
                    <input type="hidden" name="userid">
                    <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">اغلاق</button><button class="btn btn-danger" type="submit">حذف</button></div>
                </form>
            </div>
        </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script>
        $('button[data-bs-target="#update"]').on('click', function() {
            // Get the user ID from the data attribute.
            var user_id = $(this).data('user-id');
            console.log(user_id);
            // Set the value of all input fields with the name "userid" to the user ID.
            $('input[name="userid"]').each(function() {
                $(this).val(user_id);
            });
        });

        $('button[data-bs-target="#confirmation"]').on('click', function() {
            // Get the user ID from the data attribute.
            var user_id = $(this).data('user-id');
            console.log(user_id);
            // Set the value of all input fields with the name "userid" to the user ID.
            $('input[name="userid"]').each(function() {
                $(this).val(user_id);
            });
        });
    </script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="assets/js/theme.js"></script>
</body>

</html>